
var express = require('express');
var app = express();
var port = 8080;

// start te server
app.listen(port, function() {
	console.log('app started');
});

// route for homepage
app.get('/', function(req, res) {
	res.send('ldap module');
});

// route for about page
app.get('/about', function(req, res) {
	res.send('about page');
});

app.get('/contact');
app.post('/contact');